package com.clinic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.clinic.model.HealthDataEvent;
import com.clinic.service.IPersonService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Validated
@Slf4j
public class RestApi {

    private final static String URI_API = "/api/v1";
    private final static String URI_PERSON = "/person";

    @Autowired
	private IPersonService personService;
    
    @PostMapping(value = URI_API + URI_PERSON + "/info")
    public ResponseEntity<String> createPersonHealthDataRB(@RequestBody HealthDataEvent event) {
        log.info("Inserting person health data for sleep quality: {}, and slin condition: {} ",event.getSleepQuality(),event.getSkinCondition());
        personService.createPersonHealthData(event);
        return ResponseEntity.ok("Person health data was inserted");
    }


}
