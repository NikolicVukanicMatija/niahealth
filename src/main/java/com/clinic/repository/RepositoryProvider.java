package com.clinic.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RepositoryProvider {
	
	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	private PersonHealtsRepository personHealtsRepository;
	
	public PersonRepository getPersonRepository() {
	    return personRepository;
	  }
	
	public PersonHealtsRepository getPersonHealtsRepository() {
	    return personHealtsRepository;
	  }

}
