package com.clinic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.clinic.model.PersonHealts;

@Repository
public interface PersonHealtsRepository extends JpaRepository<PersonHealts, Long>  {

}
