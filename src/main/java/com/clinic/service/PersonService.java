package com.clinic.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.clinic.exception.NullPointValidation;
import com.clinic.exception.SkinConditionsViolationException;
import com.clinic.exception.SleepQualityViolationException;
import com.clinic.model.HealthDataEvent;
import com.clinic.model.Person;
import com.clinic.model.PersonHealts;
import com.clinic.repository.RepositoryProvider;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class PersonService implements IPersonService {

	
    @Autowired
    private final RepositoryProvider repositoryProvider;

    @Value("${range.start}")
    Integer rangeStart;

    @Value("${range.end}")
    Integer rangeEnd;
     
    @Override
    public void createPersonHealthData(HealthDataEvent event) {
    	verifyRange(event.getSleepQuality(),event.getSkinCondition());
    	verifyValue(event.getSleepQuality());
    	verifyValue(event.getSkinCondition());
    	Person person = new Person();
    	List<PersonHealts> results = new ArrayList<>();
    	PersonHealts hd =  new PersonHealts();
    	if(event.getId()!=null) {
    	person.setId(event.getId());	
    	}
    	hd.setSkinCondition(event.getSkinCondition());
    	hd.setSleepQuality(event.getSleepQuality());
    	results.add(hd);
    	person.setHealts(results); 
    	repositoryProvider.getPersonRepository().save(person);
    }
    private void verifyRange(Integer sleepQuality, Integer skinCondition) {
    	 if (sleepQuality < rangeStart || sleepQuality > rangeEnd)
             throw new SleepQualityViolationException(sleepQuality);
    	 if (skinCondition < rangeStart || skinCondition > rangeEnd)
             throw new SkinConditionsViolationException(skinCondition);
	}
    private void verifyValue(Integer value) {
        if (value == null ) throw new NullPointValidation(value);
    }


}
