package com.clinic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "person_healths")
public class PersonHealts {
	
	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @Column(nullable = false)
	  private Long id;

	
	  @Column(name = "sleep_quality", nullable = false)
	  private Integer sleepQuality;
	  
	  @Column(name = "skin_condition", nullable = false)
	  private Integer skinCondition;
	    
	  @ManyToOne
	  @JoinColumn(name = "person_id", insertable = false, updatable = false)
	  private Person person;
	 


}
