package com.clinic.model;

import lombok.Data;

@Data
public class HealthDataEvent {
	
	private Long id;
	private Integer sleepQuality;
	private Integer skinCondition;

}
