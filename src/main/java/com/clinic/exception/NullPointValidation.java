package com.clinic.exception;

public class NullPointValidation extends RuntimeException {
	
	public NullPointValidation(Integer value) {
        super("Skin condition/ sleep quality has null value: " + value);
    }}
