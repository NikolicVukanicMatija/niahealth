package com.clinic.exception;

public class SleepQualityViolationException extends RuntimeException {
	
	public SleepQualityViolationException(Integer sleepQuality) {
        super("Sleep quality violation for range: " + sleepQuality);
    }

}
