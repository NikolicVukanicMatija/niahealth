package com.clinic.exception;

public class SkinConditionsViolationException extends RuntimeException {
	
	public SkinConditionsViolationException(Integer skinCondition) {
        super("Skin condition violation for range: " + skinCondition);
    }

}
