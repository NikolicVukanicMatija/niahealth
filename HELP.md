Application will run on 8080 port

To try restapi use curl command:

curl --location --request POST 'http://localhost:8080//api/v1/person/info' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id":null,
    "sleepQuality":6,
    "skinCondition":6

}'

where id in body represent patient id. When id is null new patient will be created and health data will be inserted in child table. Adding new values for existing patient will be possible with providing patient id in body.

Db for this example is MySQL. Please adapt your credentials for local db. test.db is on root path.

TODO:
*add timestamp on entities
*code commenting
*jUnit and integration tests